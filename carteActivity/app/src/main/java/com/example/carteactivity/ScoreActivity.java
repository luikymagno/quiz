package com.example.carteactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    private Intent info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        info = getIntent();
        this.displayInfo();
    }

    private void displayInfo() {
        int rightAnswers = info.getIntExtra("rightAnswers", 0);
        int questionCount = info.getIntExtra("questionCount", 0);
        SharedPreferences sp = getSharedPreferences("score_info", Context.MODE_PRIVATE);

        TextView text = findViewById(R.id.score);
        String message = String.format("Vous avez marqué %d points sur %d\n", rightAnswers, questionCount);

        if(sp.contains("rightAnswers") && sp.contains("questionCount")) {
            int previousRightAnswers = sp.getInt("rightAnswers", -1);
            String name = sp.getString("name", "");
            if(rightAnswers > previousRightAnswers) {
                Button button = findViewById(R.id.save);
                button.setTag("save");
                message += String.format("Le record précédent établi par %s était %d / %d.\n" +
                                "Vous battez ce record !\nEntrez votre pseudo pour remplacer l'ancien record :",
                        name, previousRightAnswers, questionCount);
            }
            else if(rightAnswers == previousRightAnswers) {
                Button button = findViewById(R.id.save);
                button.setTag("save");
                message += String.format("Le record précédent établi par %s était %d / %d.\n" +
                                "Vous égalez ce record !\nEntrez votre pseudo pour remplacer l'ancien record :",
                        name, previousRightAnswers, questionCount);
            }
            else if(rightAnswers < previousRightAnswers) {
                Button button = findViewById(R.id.save);
                button.setTag("dont_save");
                message += String.format("Le record est actuellement détenu par %s avec %d / %d.\nVous êtes en dessous...",
                        name, previousRightAnswers, questionCount);
            }
        }
        else {
            Button button = findViewById(R.id.save);
            button.setTag("save");
            message += ("Vous êtes le premier joueur à terminer le quiz." +
                    "Vous établissez le premier record !" +
                    "Entrez votre pseudo pour enregistrer ce record :");
        }
        text.setText(message);
    }

    public void onClick(View view) {
        int rightAnswers = info.getIntExtra("rightAnswers", 0);
        int questionCount = info.getIntExtra("questionCount", 0);
        if ( ( (Button) view).getTag().toString() == "save" ) {
            EditText editText = findViewById(R.id.input_name);
            String name = String.valueOf(editText.getText());
            SharedPreferences sp = getSharedPreferences("score_info", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putString("name", name);
            editor.putInt("rightAnswers", rightAnswers);
            editor.putInt("questionCount", questionCount);
            editor.apply();
        }
        this.changeContext(rightAnswers, questionCount);
    }

    private void changeContext(int rightAnswers, int questionCount) {
        Intent sms = new Intent();
        sms.setClass(this, FinActivity.class);
        sms.putExtra("rightAnswers", rightAnswers);
        sms.putExtra("questionCount", questionCount);
        startActivity(sms);
        finish();
    }
}
