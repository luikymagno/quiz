package com.example.carteactivity;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CarteActivity extends AppCompatActivity implements View.OnClickListener {

    //Storage the current card
    private Carte carte;
    //Iterator to access the next card
    private Iterator index;
    //Storage the player
    private MediaPlayer song;
    //Storage the current quiz
    private Quiz quiz;
    //
    private int rightAnswers;
    private int questionCount;

    /**
     * Constructor of CarteActivity
     * Set the attribute carte as the first card on the quiz and display it
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quiz = new Quiz();
        index = quiz.getCartesIterator();
        carte = (Carte) index.next();
        song = MediaPlayer.create(this, R.raw.pink_panther);
        rightAnswers = 0;
        this.setCard(carte);
        questionCount = 1;
    }

    /**
     * Display a card
     * @param card
     */
    private void setCard(Carte card) {
        //Display the question
        TextView question = findViewById(R.id.question);
        question.setText(card.getQuestion());

        //Create a vector including all the responses from card and shuffle them
        Vector<String> responses = card.getPropositionsIncorrectes();
        responses.add(card.getReponseCorrecte());
        Collections.shuffle(responses);

        //For each response, create a button and add it to the layout
        LinearLayout layout = findViewById(R.id.myLayout);
        Button b;
        for (int i = 0; i < responses.size(); i++) {
            b = new Button(this);
            b.setText(responses.get(i));
            //Set a tag informing wheater the response is correct or not
            if(b.getText() == card.getReponseCorrecte()) {
                b.setTag("correct");
            }
            else {
                b.setTag("incorrect");
            }
            b.setOnClickListener(this);
            layout.addView(b);
        }
    }

    /**
     * Display a toast regarding to the tag of the clicked button
     * Display the next card if there is
     * @param v
     */
    public void onClick(View v) {
        String msg = "";
        if( ( (Button) v).getTag().toString() == "correct") {
            msg = "Bravo !";
            rightAnswers++;
        }
        else if ( ( (Button) v).getTag().toString() == "incorrect"){
            msg = "incorrect...";
        }
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        //Erase the previous content
        this.erase();
        if(index.hasNext()) {
            carte = (Carte) index.next();
            this.setCard(carte);
            questionCount++;
        }
        else {
            this.end();
        }
    }

    /**
     * Erase the current displayed content
     */
    private void erase() {
        TextView question = findViewById(R.id.question);
        question.setText("");
        LinearLayout layout = findViewById(R.id.myLayout);
        //Remove all the views unless the question
        layout.removeViews(1, layout.getChildCount()-1);

    }

    /**
     * Create a delay between the toast from the last answer and the one informing the quiz end
     * Change the context to ScoreActivity
     */
    private void end() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText( CarteActivity.this,
                        "Fin du Quiz", Toast.LENGTH_SHORT).show();
            }
        },1000);
        song.stop();

        changeContext();
    }

    /**
     * Send to ScoreActivity the number of question and the score
     * Call ScoreActivity
     * End CarteActivity
     */
    private void changeContext() {
        Intent displayScore = new Intent();
        displayScore.setClass(this, ScoreActivity.class);
        displayScore.putExtra("rightAnswers", rightAnswers);
        displayScore.putExtra("questionCount", questionCount);
        startActivity(displayScore);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        song.start();
    }

    @Override
    protected void onStop() {
        song.pause();
        super.onStop();
    }
}
