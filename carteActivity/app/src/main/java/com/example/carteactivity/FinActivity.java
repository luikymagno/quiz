package com.example.carteactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class FinActivity extends AppCompatActivity {
    private Intent sms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin);

        sms = getIntent();
    }

    public void sendMessage(View view) {
        Intent msg = new Intent(Intent.ACTION_SENDTO);
        msg.setData(Uri.parse("smsto:"));
        int rightAnswers = sms.getIntExtra("rightAnswers", 0);
        int questionCount = sms.getIntExtra("questionCount", 0);
        String msgContent = String.format("Salut !, J'ai marqué %d points en %d cartes sur l'app de quiz !",
                rightAnswers, questionCount);
        msg.putExtra("sms_body", msgContent);
        startActivity(msg);
    }

    public void quit(View view) {
        finish();
    }
}
